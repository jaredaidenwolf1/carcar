from django.db import models
from django.urls import reverse


class SalesVinVO(models.Model):
    vinVO = models.CharField(
        max_length=17,
        unique=True,
    )
    import_id = models.PositiveSmallIntegerField(
        default=0,
        null=True,
        blank=True,
    )


class ServiceTechnician(models.Model):
    technician_name = models.CharField(
        max_length=200,
    )
    employee_number = models.CharField(
        max_length=8,
        unique=True,
    )

    def __str__(self):
        return self.technician_name

    def get_api_url(self):
        return reverse(
            "api_show_technician",
            kwargs={"pk": self.pk},
        )


class ServiceAppointment(models.Model):
    vin = models.CharField(
        max_length=17,
        null=True,
        blank=True,
    )
    customer_name = models.CharField(
        max_length=200,
    )
    vip = models.BooleanField(
        default=False,
        blank=True,
        null=True,
    )
    appointment_datetime = models.DateTimeField(
        null=True,
        blank=True,
    )
    technician = models.ForeignKey(
        ServiceTechnician,
        related_name="appointment",
        on_delete=models.PROTECT,
    )
    service_reason = models.TextField()

    submitted = "Submitted"
    canceled = "Canceled"
    finished = "Finished"
    statuses = (
        (submitted, "Submitted"),
        (canceled, "Canceled"),
        (finished, "Finished"),
    )
    appointment_status = models.CharField(
        max_length=10,
        choices=statuses,
        default="Submitted",
    )

    def __str__(self):
        return self.customer_name

    def get_api_url(self):
        return reverse(
            "api_show_appointment",
            kwargs={"pk": self.pk},
        )
