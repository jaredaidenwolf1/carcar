import json

from common.json import ModelEncoder
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods

from .models import SalesVinVO, ServiceAppointment, ServiceTechnician


class SalesVinVODetailEncoder(ModelEncoder):
    model = SalesVinVO
    properties = [
        "id",
        "vinVO",
    ]


class ServiceTechnicianListEncoder(ModelEncoder):
    model = ServiceTechnician
    properties = [
        "id",
        "technician_name",
        "employee_number",
    ]


class ServiceTechnicianDetailEncoder(ModelEncoder):
    model = ServiceTechnician
    properties = [
        "id",
        "technician_name",
        "employee_number",
    ]


class ServiceAppointmentListEncoder(ModelEncoder):
    model = ServiceAppointment
    properties = [
        "id",
        "vin",
        "customer_name",
        "vip",
        "appointment_datetime",
        "technician",
        "service_reason",
        "appointment_status",
    ]
    encoders = {
        "technician": ServiceTechnicianListEncoder(),
    }


class ServiceAppointmentDetailEncoder(ModelEncoder):
    model = ServiceAppointment
    properties = [
        "id",
        "vin",
        "customer_name",
        "vip",
        "appointment_datetime",
        "technician",
        "service_reason",
        "appointment_status",
    ]
    encoders = {
        "technician": ServiceTechnicianListEncoder(),
    }


@require_http_methods(["GET", "POST"])
def api_list_service_technicians(request):
    if request.method == "GET":
        service_technicians = ServiceTechnician.objects.all()
        return JsonResponse(
            {"technician": service_technicians},
            encoder=ServiceTechnicianListEncoder,
        )
    else:
        content = json.loads(request.body)
        service_technicians = ServiceTechnician.objects.create(**content)
        return JsonResponse(
            service_technicians,
            encoder=ServiceTechnicianDetailEncoder,
            safe=False,
        )


@require_http_methods(["GET", "DELETE", "PUT"])
def api_show_service_technician(request, pk):
    if request.method == "GET":
        service_technician = ServiceTechnician.objects.get(id=pk)
        return JsonResponse(
            service_technician,
            encoder=ServiceTechnicianDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = ServiceTechnician.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        ServiceTechnician.objects.filter(id=pk).update(**content)
        service_technician = ServiceTechnician.objects.get(id=pk)
        return JsonResponse(
            service_technician,
            encoder=ServiceTechnicianDetailEncoder,
            safe=False,
        )


@require_http_methods(["GET", "POST"])
def api_list_service_appointments(request):
    if request.method == "GET":
        service_appointments = ServiceAppointment.objects.all()
        return JsonResponse(
            {"appointments": service_appointments},
            encoder=ServiceAppointmentListEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            service_technician = ServiceTechnician.objects.get(id=content["technician_id"])
            content["technician"] = service_technician

            try:
                vipVIN = SalesVinVO.objects.get(vinVO=content["vin"])
                if vipVIN:
                    content["vip"] = True
            except SalesVinVO.DoesNotExist:
                content["vip"] = False

        except ServiceTechnician.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid service technician id"},
                status=400,
            )
        service_appointment = ServiceAppointment.objects.create(**content)
        return JsonResponse(
            service_appointment,
            encoder=ServiceAppointmentDetailEncoder,
            safe=False,
        )


@require_http_methods(["GET", "PUT"])
def api_show_service_appointment(request, pk):
    if request.method == "GET":
        service_appointment = ServiceAppointment.objects.get(id=pk)
        return JsonResponse(
            service_appointment,
            encoder=ServiceAppointmentDetailEncoder,
            safe=False,
        )
    else:
        content = json.loads(request.body)
        if "technician" in content:
            technician = ServiceTechnician.objects.get(id=content["technician"])
            content["technician"] = technician
        ServiceAppointment.objects.filter(id=pk).update(**content)
        service_appointment = ServiceAppointment.objects.get(id=pk)
        return JsonResponse(
            service_appointment,
            encoder=ServiceAppointmentDetailEncoder,
            safe=False,
        )
