from django.urls import path

from .views import (
    api_show_service_appointment,
    api_list_service_appointments,
    api_show_service_technician,
    api_list_service_technicians,
)

urlpatterns = [
    path(
        "appointments/",
        api_list_service_appointments,
        name="api_list_service_appointments",
    ),
    path(
        "appointments/<int:pk>/",
        api_show_service_appointment,
        name="api_show_service_appointment",
    ),
    path(
        "technicians/",
        api_list_service_technicians,
        name="api_list_service_technicians",
    ),
    path(
        "technicians/<int:pk>/",
        api_show_service_technician,
        name="api_show_service_technician",
    ),
]
