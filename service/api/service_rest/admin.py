from django.contrib import admin
from .models import (
    SalesVinVO,
    ServiceTechnician,
    ServiceAppointment,
)


@admin.register(SalesVinVO)
class SalesVinVOAdmin(admin.ModelAdmin):
    pass


@admin.register(ServiceTechnician)
class ServiceTechnicianAdmin(admin.ModelAdmin):
    pass


@admin.register(ServiceAppointment)
class ServiceAppointmentAdmin(admin.ModelAdmin):
    pass
