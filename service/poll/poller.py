import json
import os
import sys
import time

import django
import requests

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "service_project.settings")
django.setup()

from service_rest.models import SalesVinVO


def get_sales_vin():
    response = requests.get("http://inventory-api:8000/api/automobiles/")
    content = json.loads(response.content)
    for auto in content["autos"]:
        SalesVinVO.objects.update_or_create(
            import_id=auto["id"],
            defaults={
                "vinVO": auto["vin"],
            },
        )


def poll():
    while True:
        print("Service poller polling for data")
        try:
            get_sales_vin()
        except Exception as e:
            print(e, file=sys.stderr)
        time.sleep(60)


if __name__ == "__main__":
    poll()
