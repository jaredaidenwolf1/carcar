import { NavLink } from 'react-router-dom';


function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">CarCar</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <li className="nav-item">
              <NavLink className="nav-link active" aria-current="page" to="/">Home</NavLink>
            </li>
            <li className="nav-item dropdown">
              <a className="nav-link dropdown-toggle" data-bs-toggle="dropdown" href="#" role="button" aria-expanded="false">Inventory</a>
              <ul className="dropdown-menu">
                <li><NavLink className="dropdown-item" to="/manufacturers">Manufacturer List</NavLink></li>
                <li><NavLink className="dropdown-item" to="/manufacturers/new">Add a Manufacturer</NavLink></li>
                <li><NavLink className="dropdown-item" to="/models">Vehicle Models</NavLink></li>
                <li><NavLink className="dropdown-item" to="/models/new">Add a Vehicle Model</NavLink></li>
                <li><NavLink className="dropdown-item" to="/automobiles">Automobile</NavLink></li>
                <li><NavLink className="dropdown-item" to="/automobiles/new">Add an Automobile</NavLink></li>
              </ul>
            </li>
            <li className="nav-item dropdown">
              <a className="nav-link dropdown-toggle" data-bs-toggle="dropdown" href="#" role="button" aria-expanded="false">Sales</a>
              <ul className="dropdown-menu">
                <li><NavLink className="dropdown-item" to="/customer/new">Add New Customer</NavLink></li>
                <li><NavLink className="dropdown-item" to="/salesperson/new">Add Sales Person</NavLink></li>
                <li><NavLink className="dropdown-item" to="/salesperson/history">Sales History</NavLink></li>
                <li><NavLink className="dropdown-item" to="/salesrecord/new">Add a Sales Record</NavLink></li>
                <li><NavLink className="dropdown-item" to="/salesrecord/all">All Sales Records</NavLink></li>
              </ul>
            </li>
            <li className="nav-item dropdown">
              <a className="nav-link dropdown-toggle" data-bs-toggle="dropdown" href="#" role="button" aria-expanded="false">Service</a>
              <ul className="dropdown-menu">
                <li><NavLink className="dropdown-item" to="/technicians/">List Technicians</NavLink></li>
                <li><NavLink className="dropdown-item" to="/technicians/new">Add New Technician</NavLink></li>
                <li><NavLink className="dropdown-item" to="/appointments">List Appointments</NavLink></li>
                <li><NavLink className="dropdown-item" to="/appointments/new">Add New Appointment</NavLink></li>
                <li><NavLink className="dropdown-item" to="/appointments/history">Service Appointment History</NavLink></li>
              </ul>
            </li>
          </ul>
        </div>
      </div>
    </nav >
  )
}

export default Nav;
