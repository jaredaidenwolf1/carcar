import React from 'react';

class ServiceAppointmentList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            service_appointments: [],
        };
        this.handleCancelServiceAppointment = this.handleCancelServiceAppointment.bind(this);
        this.handleFinishServiceAppointment = this.handleFinishServiceAppointment.bind(this);
    }

    async componentDidMount() {
        const url = 'http://localhost:8080/api/appointments/';
        try {
            const response = await fetch(url);
            if (response.ok) {
                const data = await response.json();
                const serviceAppointmentsArray = data.appointments;
                const submittedServiceAppointments = serviceAppointmentsArray.filter(submittedAppointment => submittedAppointment.appointment_status === "Submitted");
                this.setState({ service_appointments: submittedServiceAppointments });
            }
        } catch (e) {
            console.error(e);
        }
    }

    async handleCancelServiceAppointment(service_appointment, event) {
        event.preventDefault();
        const data = { "appointment_status": "Canceled" };
        const url = `http://localhost:8080/api/appointments/${service_appointment.id}/`;
        const fetchConfig = {
            method: "PUT",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(url, fetchConfig)
        if (response.ok) {
            this.componentDidMount()
        }
    }
    async handleFinishServiceAppointment(service_appointment, event) {
        event.preventDefault();
        const data = { "appointment_status": "Finished" };
        const url = `http://localhost:8080/api/appointments/${service_appointment.id}/`;
        const fetchConfig = {
            method: "PUT",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(url, fetchConfig)
        if (response.ok) {
            this.componentDidMount()
        }
    }

    render() {
        return (
            <div className="container">
                <h2 className="mb-5 mt-5">Scheduled Service Appointments</h2>
                <table className="table table-striped align-middle mt-5">
                    <thead>
                        <tr>
                            <th>VIN</th>
                            <th>Customer Name</th>
                            <th>VIP</th>
                            <th>Date</th>
                            <th>Time</th>
                            <th>Technician</th>
                            <th>Reason</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.service_appointments.map((service_appointment) => {
                            const dateObj = new Date(service_appointment.appointment_datetime);
                            return (
                                <tr key={service_appointment.id}>
                                    <td>{service_appointment.vin}</td>
                                    <td>{service_appointment.customer_name}</td>
                                    <td className="text-success fw-bold">{(service_appointment.vip ? "VIP" : null)}</td>
                                    <td>{dateObj.toLocaleDateString()}</td>
                                    <td>{dateObj.toLocaleTimeString('en-US')}</td>
                                    <td>{service_appointment.technician.technician_name}</td>
                                    <td>{service_appointment.service_reason}</td>
                                    <td>
                                        <div className="btn-group" role="group" aria-label="Button group">
                                            <button type="button" className="btn btn-danger" onClick={this.handleCancelServiceAppointment.bind(this, service_appointment)}>Cancel</button>
                                            <button type="button" className="btn btn-success" onClick={this.handleFinishServiceAppointment.bind(this, service_appointment)}>Finished</button>
                                        </div>
                                    </td>
                                </tr>
                            );
                        })}
                    </tbody>
                </table>
            </div>
        );
    }

}

export default ServiceAppointmentList;
