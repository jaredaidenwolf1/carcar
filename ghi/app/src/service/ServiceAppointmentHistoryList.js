import React from 'react';


class AppointmentHistoryList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            serviceAppointmentHistory: [],
            filteredServiceAppointmentHistory: [],
        };
        this.handleFilterVin = this.handleFilterVin.bind(this);
    }

    async componentDidMount() {
        const url = 'http://localhost:8080/api/appointments/';

        try {
            const response = await fetch(url);
            if (response.ok) {
                const data = await response.json();
                this.setState({ serviceAppointmentHistory: data.appointments });
            }
        } catch (e) {
            console.error(e);
        }
    }


    handleFilterVin(filterVin) {
        const vin = filterVin.target.value;
        const vehicles = this.state.serviceAppointmentHistory;
        const vinHistoryArray = [];
        for (let vehicle of vehicles) {
            if (vehicle.vin === vin) {
                vinHistoryArray.push(vehicle);
            };
        }
        this.setState({
            filteredServiceAppointmentHistory: vinHistoryArray,
        });
    }


    render() {
        return (
            <div>
                <div className="row">
                    <div>
                        <h2 className="mb-5 mt-5">Service History by VIN</h2>
                    </div>
                    <input
                        value={this.filterVin}
                        onInput={this.handleFilterVin}
                        type="text"
                        id="header-search"
                        placeholder="Search service history by VIN"
                        name="s"
                    />
                </div>
                <div className="container mt-5">
                    <div>
                        <table className="table table-striped">
                            <thead>
                                <tr>
                                    <th>VIN</th>
                                    <th>Customer Name</th>
                                    <th>VIP</th>
                                    <th>Date</th>
                                    <th>Time</th>
                                    <th>Technician</th>
                                    <th>Reason</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tbody>
                                {this.state.filteredServiceAppointmentHistory.map((service_appointment) => {
                                    const dateObj = new Date(service_appointment.appointment_datetime);
                                    return (
                                        <tr key={service_appointment.id}>
                                            <td>{service_appointment.vin}</td>
                                            <td>{service_appointment.customer_name}</td>
                                            <td className="text-success fw-bold">{(service_appointment.vip ? "VIP" : null)}</td>
                                            <td>{dateObj.toLocaleDateString()}</td>
                                            <td>{dateObj.toLocaleTimeString('en-US')}</td>
                                            <td>{service_appointment.technician.technician_name}</td>
                                            <td>{service_appointment.service_reason}</td>
                                            <td>{service_appointment.appointment_status}</td>
                                        </tr>
                                    );
                                })}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        );
    }

}

export default AppointmentHistoryList;
