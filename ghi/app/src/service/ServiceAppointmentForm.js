import React from "react";
import { useNavigate } from "react-router-dom"

function withExtras(Component) {
    return (props) => (
        <Component {...props} useNavigate={useNavigate()} />
    );
}


class AddServiceAppointmentForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            vin: "",
            customer_name: "",
            appointment_datetime: "",
            technician_id: "",
            technicians: [],
            service_reason: "",
            appointment_status: "Submitted",
        };

        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChangeVin = this.handleChangeVin.bind(this);
        this.handleChangeCustomerName = this.handleChangeCustomerName.bind(this);
        this.handleChangeAppointmentDateTime = this.handleChangeAppointmentDateTime.bind(this);
        this.handleChangeTechnician = this.handleChangeTechnician.bind(this);
        this.handleChangeServiceReason = this.handleChangeServiceReason.bind(this);
    }

    async componentDidMount() {
        const technicianUrl = 'http://localhost:8080/api/technicians/';
        const response = await fetch(technicianUrl);
        if (response.ok) {
            const data = await response.json();
            this.setState({ technicians: data.technician });
        }
    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = { ...this.state };
        delete data.technicians;
        const appointmentUrl = "http://localhost:8080/api/appointments/";
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
            },
        };
        const response = await fetch(appointmentUrl, fetchConfig);
        if (response.ok) {
            this.setState({
                vin: "",
                customer_name: "",
                appointment_datetime: "",
                technician_id: "",
                service_reason: "",
            });
            this.props.useNavigate("/appointments")
        }
    }

    handleChangeVin(event) {
        const value = event.target.value;
        this.setState({ vin: value });
    }

    handleChangeCustomerName(event) {
        const value = event.target.value;
        this.setState({ customer_name: value });
    }

    handleChangeAppointmentDateTime(event) {
        const value = event.target.value;
        this.setState({ appointment_datetime: value });
    }

    handleChangeTechnician(event) {
        const value = event.target.value;
        this.setState({ technician_id: value });
    }

    handleChangeServiceReason(event) {
        const value = event.target.value;
        this.setState({ service_reason: value });
    }

    render() {
        return (
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-5">
                        <h2 className="mb-5 mt-5">Create a New Service Appointment</h2>
                        <form onSubmit={this.handleSubmit} id="create-service-appointment-form">
                            <div className="form-floating mb-3">
                                <input onChange={this.handleChangeVin} value={this.state.vin} placeholder="VIN" required type="text" name="vin" id="vin" className="form-control" />
                                <label htmlFor="vin">VIN</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleChangeCustomerName} value={this.state.customer_name} placeholder="Customer name" required type="text" name="customer_name" id="customer_name" className="form-control" />
                                <label htmlFor="customer_name">Customer name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleChangeAppointmentDateTime} value={this.state.appointment_datetime} placeholder="Appointment date and time" required type="datetime-local" name="appointment_datetime" id="appointment_datetime" className="form-control" />
                                <label htmlFor="appointment_datetime">Appointment date and time</label>
                            </div>
                            <div className="mb-3">
                                <select value={this.state.technician_id} onChange={this.handleChangeTechnician} id="technician" className="form-select"  >
                                    <option value="">Technician</option>
                                    {this.state.technicians.map(technician => {
                                        return (
                                            <option key={technician.id} value={technician.id}>{technician.technician_name}</option>
                                        )
                                    })}
                                </select>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleChangeServiceReason} value={this.state.service_reason} placeholder="Service reason" required type="text" name="service_reason" id="service_reason" className="form-control" />
                                <label htmlFor="service_reason">Service reason</label>
                            </div>
                            <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        );
    }
}

export default withExtras(AddServiceAppointmentForm);
