import React from "react";

class ManufacturerList extends React.Component {
    constructor(props) {
        super(props)
        this.state = { manufacturers: [] }
    }


    async componentDidMount() {
        const response = await fetch('http://localhost:8100/api/manufacturers/');
        if (response.ok) {
            const data = await response.json();
            this.setState({ manufacturers: data.manufacturers });
        };
    }

    render() {
        return (
            <div className="row">
                <div className="offset-1 col-8">
                    <div className="shadow p-3 mt-4">
                        <h2 className="mb-5 mt-5">Manufacturers</h2>
                        <table className="table table-striped">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                </tr>
                            </thead>
                            <tbody>
                                {this.state.manufacturers.map(manufacturer => {
                                    return (
                                        <tr key={manufacturer.id}>
                                            <td>{manufacturer.name}</td>
                                        </tr>
                                    );
                                })}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        )
    }
}



export default ManufacturerList;
