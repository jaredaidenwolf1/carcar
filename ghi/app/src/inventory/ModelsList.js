import React from 'react';

class ModelsList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            models: [],
        };
    }

    async componentDidMount() {
        const url = 'http://localhost:8100/api/models/';
        try {
            const response = await fetch(url);
            if (response.ok) {
                const data = await response.json();
                const requests = [];
                for (let model of data.models) {
                    const autoUrl = `http://localhost:8100${model.href}`;
                    requests.push(fetch(autoUrl));
                }
                const responses = await Promise.all(requests);
                const models = [];
                for (const modelResponse of responses) {
                    if (modelResponse.ok) {
                        const details = await modelResponse.json();
                        models.push(details);
                    } else {
                        console.error(modelResponse);
                    }
                }
                this.setState({ models: models });
            }
        } catch (e) {
            console.error(e);
        }
    }

    render() {
        return (
            <div className="container">
                <h2 className="mb-5 mt-5">Vehicle Models Inventory</h2>
                <table className="table table-striped align-middle mt-5">
                    <thead>
                        <tr>
                            <th>Manufacturer</th>
                            <th>Name</th>
                            <th>Picture</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.models.map((model) => {
                            return (
                                <tr key={model.id}>
                                    <td>{model.manufacturer.name}</td>
                                    <td>{model.name}</td>
                                    <td><img src={model.picture_url} alt="" /></td>
                                </tr>
                            );
                        })}
                    </tbody>
                </table>
            </div>
        );
    }

}

export default ModelsList;
