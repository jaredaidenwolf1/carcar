import React from 'react';
import { useNavigate } from "react-router-dom";

function reFresh(Component) {
    return (props) => <Component {...props} useNavigate={useNavigate()} />;
}


class AutomobileForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            color: '',
            year: '',
            vin: '',
            model_id: '',
            models: [],

        };
        this.handleChangeColor = this.handleChangeColor.bind(this)
        this.handleChangeYear = this.handleChangeYear.bind(this)
        this.handleChangeVin = this.handleChangeVin.bind(this)
        this.handleChangeModel = this.handleChangeModel.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
    }
    async componentDidMount() {
        const url = 'http://localhost:8100/api/models/';
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            this.setState({ models: data.models })
        }
    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = { ...this.state };
        delete data.models


        const url = 'http://localhost:8100/api/automobiles/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(url, fetchConfig);
        if (response.ok) {
            this.setState({
                color: '',
                year: '',
                vin: '',
                model_id: '',
            });
            this.props.useNavigate("/automobiles")
        }
    }
    handleChangeColor(event) {
        const value = event.target.value;
        this.setState({ color: value })
    }
    handleChangeYear(event) {
        const value = event.target.value;
        this.setState({ year: value })
    }
    handleChangeVin(event) {
        const value = event.target.value;
        this.setState({ vin: value })
    }
    handleChangeModel(event) {
        const value = event.target.value;
        this.setState({ model_id: value })
    }
    render() {
        return (
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h2 className="mb-5 mt-5">Add Automobile</h2>
                        <form onSubmit={this.handleSubmit} id="create-Auto-form">
                            <div className="form-floating mb-3">
                                <input onChange={this.handleChangeColor} value={this.state.color} placeholder="color" required type="text" name="color" id="color" className="form-control" />
                                <label htmlFor="color">Color</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleChangeYear} value={this.state.year} placeholder="year" required type="text" name="year" id="year" className="form-control" />
                                <label htmlFor="year">Year</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleChangeVin} value={this.state.vin} placeholder="vin" required type="text" name="vin" id="vin" className="form-control" />
                                <label htmlFor="vin">VIN</label>
                            </div>
                            <div className="mb-3">
                                <select onChange={this.handleChangeModel} value={this.state.model} required id="model" name="model" className="form-select">
                                    <option value="">Vehicle Model</option>
                                    {this.state.models.map(model => {
                                        return (
                                            <option key={model.id} value={model.id}>{model.name}</option>
                                        );
                                    })}
                                </select>
                            </div>
                            <button className="btn btn-primary" id="addAutoBtn">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        );
    }

}

export default reFresh(AutomobileForm);
