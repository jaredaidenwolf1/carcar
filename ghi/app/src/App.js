import { BrowserRouter, Routes, Route } from "react-router-dom";
import MainPage from "./MainPage";
import Nav from "./Nav";
import ModelsList from "./inventory/ModelsList";
import AutomobilesList from "./inventory/AutomobilesList";
import AddModelForm from "./inventory/ModelsForm"
import ManufacturerList from './inventory/ManufacturerList';
import ManufacturerForm from './inventory/ManufacturerForm';
import AutomobileForm from './inventory/AutomobileForm';
import ServiceTechnicianList from "./service/ServiceTechnicianList";
import ServiceTechnicianForm from "./service/ServiceTechnicianForm";
import ServiceAppointmentList from "./service/ServiceAppointmentList";
import ServiceAppointmentForm from "./service/ServiceAppointmentForm";
import ServiceAppointmentHistoryList from "./service/ServiceAppointmentHistoryList";
import CustomerForm from './sales/CustomerForm';
import SalesPersonForm from './sales/SalesPersonForm';
import SalesRecordForm from './sales/SalesRecordForm';
import SalesRecordList from './sales/SalesRecordList';
import SalesPersonHistory from './sales/SalesPersonHistory';

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="manufacturers" element={<ManufacturerList />} />
          <Route path="manufacturers/new" element={<ManufacturerForm />} />
          <Route path="models" element={<ModelsList />} />
          <Route path="models/new" element={<AddModelForm />} />
          <Route path="automobiles" element={<AutomobilesList />} />
          <Route path="automobiles/new" element={<AutomobileForm />} />
          <Route path="customer/new" element={<CustomerForm />} />
          <Route path="salesperson/new" element={<SalesPersonForm />} />
          <Route path="salesrecord/new" element={<SalesRecordForm />} />
          <Route path="salesrecord/all" element={<SalesRecordList />} />
          <Route path="salesperson/history" element={<SalesPersonHistory />} />
          <Route path="technicians" element={<ServiceTechnicianList />} />
          <Route path="technicians/new" element={<ServiceTechnicianForm />} />
          <Route path="appointments" element={<ServiceAppointmentList />} />
          <Route path="appointments/new" element={<ServiceAppointmentForm />} />
          <Route path="appointments/history" element={<ServiceAppointmentHistoryList />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
