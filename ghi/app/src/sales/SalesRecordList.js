import React from 'react'


class SalesRecordList extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            sales_records: [],
        };

    }

    async componentDidMount() {
        const response = await fetch('http://localhost:8090/api/salesrecords/');
        if (response.ok) {
            const data = await response.json();
            this.setState({ sales_records: data.sales_records });
        };
    }


    render() {
        return (
            <div className="container">
                <h2 className="mb-5 mt-5">All The Sales Records</h2>
                <div className="d-grid gap-2 d-sm-flex justify-content-sm-left">
                </div>
                <div>
                    <table className="table table-striped">
                        <thead>
                            <tr>
                                <th>VIN</th>
                                <th>Sales Person Name</th>
                                <th> Employee ID</th>
                                <th>Customer</th>
                                <th>Sold For</th>
                            </tr>
                        </thead>
                        <tbody>
                            {this.state.sales_records.map((sale) => {
                                return (
                                    <tr key={sale.id}>
                                        <td>{sale.automobile.vin}</td>
                                        <td>{sale.sales_person.name}</td>
                                        <td>{sale.sales_person.employee_number}</td>
                                        <td>{sale.customer.name}</td>
                                        <td>${sale.price}</td>
                                    </tr>
                                );
                            })}
                        </tbody>
                    </table>
                </div>
            </div>

        )
    }
}
export default SalesRecordList;