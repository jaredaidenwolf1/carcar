import React from 'react';
import { useNavigate} from "react-router-dom";

function reFresh(Component) {
    return (props) => <Component {...props} useNavigate={useNavigate()} />;
}


class SalesRecordForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            sales_person: '',
            sales_persons: [],
            customer: '',
            customers: [],
            automobile: '',
            autos: [],
            price: '',
        }

        this.handleChangeSalesPerson = this.handleChangeSalesPerson.bind(this)
        this.handleChangeAutomobile = this.handleChangeAutomobile.bind(this)
        this.handleChangeCustomer = this.handleChangeCustomer.bind(this)
        this.handleChangePrice = this.handleChangePrice.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
    }
    async componentDidMount() {
        const carUrl = 'http://localhost:8090/api/automobilevos/unsold';
        const custUrl = 'http://localhost:8090/api/salescustomer/';
        const salepUrl = 'http://localhost:8090/api/salesperson/';
        const autoresponse = await fetch(carUrl);
        const custresponse = await fetch(custUrl);
        const salespresponse = await fetch(salepUrl);
        
        if (autoresponse.ok && custresponse.ok && salespresponse.ok) {
            const cardata = await autoresponse.json();
            const custdata = await custresponse.json();
            const salespdata = await salespresponse.json();
            this.setState({autos: cardata.automobiles, sales_persons: salespdata.sales_person, customers: custdata.customers});
        };
 
    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = { ...this.state };
        delete data.autos;
        delete data.sales_persons;
        delete data.customers;

        const url = 'http://localhost:8090/api/salesrecords/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(url, fetchConfig);
            
        if (response.ok) {
            this.setState({
                sales_person: '',
                customer: '',
                automobile: '',
                price: ''
            });    
        }
        this.props.useNavigate("/salesrecord/all")

    }


    handleChangeSalesPerson(event) {
        const value = event.target.value;
        this.setState({ sales_person: value });
    }

    handleChangeAutomobile(event) {
        const value = event.target.value;
        this.setState({ automobile: value })
    }

    handleChangeCustomer(event) {
        const value = event.target.value;
        this.setState({ customer: value })
    }

    handleChangePrice(event) {
        const value = event.target.value
        this.setState({ price: value })
    }

render() {
    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h2 className="mb-5 mt-5">Create a Sales Record </h2>
                    <form onSubmit={this.handleSubmit} id="create-salesrecord-form">

                        <div className="mb-3">
                
                            <select id="sales_person" onChange={this.handleChangeSalesPerson} value={this.state.sales_person} name="sales_person" className="form-select">
                                <option value="">Choose a Salesperson</option>
                                {this.state.sales_persons.map(sales_person => {
                                    return (
                                        <option key={sales_person.id} value={sales_person.id}>{sales_person.name} </option>
                                    )
                                })}
                            </select>
                        </div>
                        {/* automobile dropdown form */}
                        <div className="mb-3">

                            <select id="autos" onChange={this.handleChangeAutomobile} value={this.state.automobile} name="automobile" className="form-select">
                                <option value="">Choose an Automobile</option>
                                {this.state.autos.map(auto=> {
                                    return (
                                        <option key={auto.vin} value={auto.vin}>{auto.vin}</option>)
                                })}
                            </select> 
                        </div>
                        {/* Customer Dropdown */}
                        <div className="mb-3">

                            <select id="customer" onChange={this.handleChangeCustomer} value={this.state.customer} name="customer" className="form-select">
                                <option value="">Choose a Customer</option>
                                {this.state.customers.map(person => {
                                    return (
                                        <option key={person.id} value={person.id}>
                                            {person.name}
                                        </option>
                                    )
                                })}
                            </select>
                        </div>
                        {/* Form component for adding in money */}
                        <div className="mb-3">
                            <input type="text" onChange={this.handleChangePrice} value={this.state.price} className="form-control" id="sales-price-input" placeholder="Sales Price" />
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
        );
    }
}

export default reFresh(SalesRecordForm);