import React from 'react'

class SalesPersonForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            name: '',
            employeeNumber: ''
        };
        this.handleChangeName = this.handleChangeName.bind(this)
        this.handleChangeEmployeeNumber = this.handleChangeEmployeeNumber.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
    }
    async handleSubmit(event) {
        event.preventDefault();
        const data = { ...this.state };
        data.employee_number = data.employeeNumber;
        delete data.employeeNumber

        const url = 'http://localhost:8090/api/salesperson/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(url, fetchConfig);
        if (response.ok) {
            const cleared = {
                name: '',
                employeeNumber: '',
            };
            this.setState(cleared);
        }
    }
    handleChangeName(event) {
        const value = event.target.value;
        this.setState({ name: value })
    }
    handleChangeEmployeeNumber(event) {
        const value = event.target.value;
        this.setState({ employeeNumber: value })
    }
    render() {
        return (
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h2 className="mb-5 mt-5">Add a Sales Person</h2>
                        <form onSubmit={this.handleSubmit} id="create-bin-form">
                            <div className="form-floating mb-3">
                                <input onChange={this.handleChangeName} value={this.state.name} required type="text" name="name" id="name" className="form-control" />
                                <label>Name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleChangeEmployeeNumber} value={this.state.employeeNumber} required type="text" name="employee_number" id="employee_number" className="form-control" />
                                <label>Employee Number</label>
                            </div>
                            <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
            </div>

        );
    }
}
export default SalesPersonForm;