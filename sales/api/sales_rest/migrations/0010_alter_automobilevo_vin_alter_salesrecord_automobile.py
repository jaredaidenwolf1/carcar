# Generated by Django 4.0.3 on 2022-12-12 23:10

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('sales_rest', '0009_alter_automobilevo_vin'),
    ]

    operations = [
        migrations.AlterField(
            model_name='automobilevo',
            name='vin',
            field=models.CharField(max_length=17, unique=True),
        ),
        migrations.AlterField(
            model_name='salesrecord',
            name='automobile',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='sales_record', to='sales_rest.automobilevo', unique=True),
        ),
    ]
