from django.db import models
from django.urls import reverse



class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)
    import_href = models.CharField(max_length=200, unique=True)


    def get_api_url(self):
        return reverse("api_automobile", kwargs={"vin":self.vin})


    def __str__(self):
        return self.vin


class SalesPerson(models.Model):
    name = models.CharField(max_length=200)
    employee_number = models.PositiveIntegerField(unique=True)

    def __str__(self):
        return self.name

    def get_api_url(self):
        return reverse("api_sales_person", kwargs={"pk": self.pk})

    class Meta:
        ordering = ("name",)


class PotentialCustomer(models.Model):
    name = models.CharField(max_length=200)
    address = models.CharField(max_length=200)
    phone_number = models.CharField(max_length=50)

    def __str__(self):
        return self.name

    class Meta:
        constraints = [
            models.UniqueConstraint(fields=['name', 'address'], name='unique_customers')
        ]

class SalesRecord(models.Model):
    price = models.PositiveIntegerField()
    automobile = models.ForeignKey(
        AutomobileVO,
        related_name="sales_record",
        unique=True,
        on_delete=models.PROTECT
    )
    sales_person = models.ForeignKey(
        SalesPerson,
        related_name="sales_record",
        on_delete=models.PROTECT
    )
    customer = models.ForeignKey(
        PotentialCustomer,
        related_name="sales_record",
        on_delete=models.PROTECT
    )

    def __str__(self):
        return f"{self.sales_person}"
