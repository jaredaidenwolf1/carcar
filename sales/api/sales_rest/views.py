from django.shortcuts import render
from .models import SalesPerson, PotentialCustomer, SalesRecord, AutomobileVO
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
import json
from .encoders import(
    SalesPersonEncoder,
    PotentialCustomerEncoder,
    SalesRecordEncoder,
    AutomobileVOEncoder
)

@require_http_methods(["GET", "PUT"])
def api_automobile_vos(request):
    if request.method=="GET":
        automobile = AutomobileVO.objects.all()
        return JsonResponse(
            {"automobile": automobile},
            encoder=AutomobileVOEncoder
        )
    else:
        content = json.loads(request.body)
        auto = AutomobileVO.objects.get(vin=content["vin"])
        return JsonResponse(
            auto,
            encoder=AutomobileVOEncoder,
            safe=False,
        )

@require_http_methods(["GET"])
def api_unsold_automobile_vos(request):
    if request.method == "GET":
        sold_autos = [sale.automobile.vin for sale in SalesRecord.objects.all()]
        print(sold_autos, "line 36--------------")
      
        unsold_autos = AutomobileVO.objects.exclude(vin__in=sold_autos)
        print(unsold_autos, "line 38--------------")
        return JsonResponse(
            {"automobiles": unsold_autos},
            encoder=AutomobileVOEncoder,
            safe=False
        )

#create salesperson
@require_http_methods(["GET", "POST"])
def api_list_salesperson(request):
    if request.method == "GET":
        sales_person = SalesPerson.objects.all()
        return JsonResponse(
            {"sales_person": sales_person},
            encoder=SalesPersonEncoder,
        )

    else:
        try:
            content = json.loads(request.body)
            sales_person = SalesPerson.objects.create(**content)
            return JsonResponse(
                sales_person,
                encoder=SalesPersonEncoder,
                safe=False
            )
        except:
            response = JsonResponse(
                {"message": "Employee Id exists already"}
            )
            response.status_code = 500
            return response



#edit salesperson
@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_salesperson(request,pk):
    if request.method == "GET":
        sales_person = SalesPerson.objects.get(id=pk)
        return JsonResponse(
            sales_person,
            encoder=SalesPersonEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = SalesPerson.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        SalesPerson.objects.filter(id=pk).update(**content)
        sales_person = SalesPerson.objects.get(id=pk)
        return JsonResponse(
            sales_person,
            encoder=SalesPersonEncoder,
            safe=False,
        )

#create customer
@require_http_methods(["GET", "POST"])
def api_list_customer(request):
    if request.method == "GET":
        customers = PotentialCustomer.objects.all()
        return JsonResponse(
            {"customers": customers},
            encoder=PotentialCustomerEncoder,
        )

    else:
        content = json.loads(request.body)
        print(content)
        try:
            customer = PotentialCustomer.objects.create(**content)
            return JsonResponse(
                customer,
                encoder=PotentialCustomerEncoder,
                safe=False
            )
        except:
            return JsonResponse(
                {"message": "Customer cannot be created"},
                status=400,
            )

#edit customer
@require_http_methods(["GET", "PUT", "DELETE"])
def api_show_customer(request, pk):
    if request.method == "GET":
        customer = PotentialCustomer.objects.get(id=pk)
        return JsonResponse(
            customer,
            encoder=PotentialCustomerEncoder,
            safe=False,
        )

    elif request.method == "DELETE":
        count, _ = PotentialCustomer.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
    
    elif request.method == "PUT":
        content = json.loads(request.body)
        PotentialCustomer.objects.filter(id=pk).update(**content)
        customer = PotentialCustomer.objects.get(id=pk)
        return JsonResponse(
            customer,
            encoder=PotentialCustomerEncoder,
            safe=False,
        )


#creare salesrecord
@require_http_methods(["GET", "POST"])
def api_list_salesrecord(request):
    if request.method == "GET":
        sales_records = SalesRecord.objects.all()
        return JsonResponse(
            {"sales_records": sales_records},
            encoder=SalesRecordEncoder,
        )
        
    else:
        try:
            content = json.loads(request.body)

            vin = content["automobile"]
            automobile = AutomobileVO.objects.get(vin=vin)
            content["automobile"] = automobile

            sales_person_id = content["sales_person"]
            sales_person = SalesPerson.objects.get(id=sales_person_id)
            content["sales_person"] = sales_person
            
            customer_id = content["customer"]
            customer = PotentialCustomer.objects.get(id=customer_id)
            content["customer"] = customer

            print(content)
            sales_record = SalesRecord.objects.create(**content)
           
            return JsonResponse(
                sales_record,
                encoder=SalesRecordEncoder,
                safe=False
            )
        except:
            response = JsonResponse(
            {"message": "Unable to make Sales record"},
            status=400,
            )   
            return response

    
# need to implement: show available cars (vin) (create a filter function?), marked "sold"= true when you click submit.
#for all cars in inventory or automobilevo,
# if car.sold ==  False, AutomobileVO.objects.get(car)

#if (response.ok) {
#   setAttr(auto, sold, True)
#auto.delete()???

# }

# salesrecord details

@require_http_methods(["GET", "PUT", "DELETE"])
def api_show_salesrecord(request, pk):
    if request.method == "GET":
        sales_record = SalesRecord.objects.get(id=pk)
        return JsonResponse(
            sales_record,
            encoder=SalesRecordEncoder,
            safe=False,
        )

    elif request.method == "DELETE":
        count, _ = SalesRecord.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
    
    elif request.method == "PUT":
        content = json.loads(request.body)
        try:
            if "automobile" in content:
                automobile = AutomobileVO.objects.get(vin=content["automobile"])
                content["automobile"] = automobile
        except AutomobileVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid automobile vin"},
                status=404,
            )
        try:
            if "sales_person" in content:
                salesperson = SalesPerson.objects.get(id=content["sales_person"])
                content["sales_person"] = salesperson
        except SalesPerson.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid salesperson id"},
                status=404,
            )
        try:
            if "customer" in content:
                customer = PotentialCustomer.objects.get(id=content["customer"])
                content["customer"] = customer
        except PotentialCustomer.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid customer id"},
                status=404,
            )


        SalesRecord.objects.filter(id=pk).update(**content)
        sales_record = SalesRecord.objects.get(id=pk)
        return JsonResponse(
            sales_record,
            encoder=SalesRecordEncoder,
             safe=False,
        )

            


 #salesperson sales history
@require_http_methods(["GET"])
def api_salesperson_salesrecord(request, employee_pk=None):
    sales = SalesRecord.objects.all()
    filtered_sales = filter(
        lambda sales_record: sales_record.sales_person.id == employee_pk, sales)
    return JsonResponse(
        {"sales": list(filtered_sales)},
        encoder=SalesRecordEncoder,
        status=200,
    )







