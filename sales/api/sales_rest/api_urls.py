from django.urls import path

from .views import (
    api_list_salesperson,
    api_show_salesperson,
    api_list_customer,
    api_show_customer,
    api_list_salesrecord,
    api_show_salesrecord,
    api_automobile_vos,
    api_unsold_automobile_vos,
    api_salesperson_salesrecord

)

urlpatterns = [
    path("salesperson/", api_list_salesperson, name="api_list_salesperson"),
    path("salesperson/<int:pk>/", api_show_salesperson, name="api_show_salesperson"),
    path("salescustomer/", api_list_customer, name="api_list_customer"),
    path("salescustomer/<int:pk>/", api_show_customer, name="api_show_customer"),
    path("salesrecords/", api_list_salesrecord, name="api_list_salesrecord"),
    path("salesrecords/<int:pk>/", api_show_salesrecord, name="api_show_salesrecord"),
    path("automobilevos/", api_automobile_vos, name="api_automobile_vos"),
    path("automobilevos/unsold", api_unsold_automobile_vos, name="api_unsold_automobile_vos"),
    path("salesrecords/salesperson/history/<int:employee_pk>/", api_salesperson_salesrecord, name="api_salesperson_salesrecord")
]
